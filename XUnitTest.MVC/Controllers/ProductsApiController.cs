﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using XUnitTest.MVC.GR;
using XUnitTest.MVC.Models;

namespace XUnitTest.MVC.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsApiController : ControllerBase
    {
        private readonly IGenercRepository<Product> _genercRepository;

        public ProductsApiController(IGenercRepository<Product> genercRepository)
        {
            _genercRepository = genercRepository;
        }

        // GET: api/ProductsApi
        [HttpGet]
        public IActionResult GetProducts()
        {
            var products = _genercRepository.GetAll();

            return Ok(products);
        }

        // GET: api/ProductsApi/5
        [HttpGet("{id}")]
        public IActionResult GetProduct(int id)
        {
            var product = _genercRepository.GetById(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // PUT: api/ProductsApi/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public IActionResult PutProduct(int id, Product product)
        {
            if (id != product.ProductId)
            {
                return BadRequest();
            }

            _genercRepository.Update(product);

            return NoContent();
        }

        // POST: api/ProductsApi
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public IActionResult PostProduct(Product product)
        {
            _genercRepository.Add(product);

            return CreatedAtAction("GetProduct", new { id = product.ProductId }, product);
        }

        // DELETE: api/ProductsApi/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var product = _genercRepository.GetById(id);
            if (product == null)
            {
                return NotFound();
            }

            _genercRepository.Delete(product);

            return NoContent();
        }

        private bool ProductExists(int id)
        {
            var product = _genercRepository.GetById(id);

            if (product == null)
            {
                return false;
            }

            return true;
        }
    }
}
