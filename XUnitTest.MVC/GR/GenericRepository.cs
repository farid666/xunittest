﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using XUnitTest.MVC.Models;

namespace XUnitTest.MVC.GR
{
    public class GenericRepository<T> : IGenercRepository<T> where T : class
    {

        private MvcDbContext _mvcDbContext;
        private DbSet<T> _dbSet;

        public GenericRepository(MvcDbContext mvcDbContext)
        {
            _mvcDbContext = mvcDbContext;
            _dbSet = _mvcDbContext.Set<T>();
        }

        public void Add(T entity)
        {
            _dbSet.Add(entity);
            _mvcDbContext.SaveChanges();
        }

        public void Delete(T entity)
        {
            _dbSet.Remove(entity);
            _mvcDbContext.SaveChanges();
        }

        public List<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T GetById(int productId)
        {
            return _dbSet.Find(productId);
        }

        public void Update(T entity)
        {
            _dbSet.Update(entity);
            _mvcDbContext.SaveChanges();
        }
    }
}
