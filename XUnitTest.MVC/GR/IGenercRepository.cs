﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace XUnitTest.MVC.GR
{
    public interface IGenercRepository<T> where T:class
    {
        List<T> GetAll();
        T GetById(int productId);
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);

    }
}
