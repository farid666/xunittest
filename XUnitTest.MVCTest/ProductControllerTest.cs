﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using XUnitTest.MVC.Controllers;
using XUnitTest.MVC.GR;
using XUnitTest.MVC.Models;

namespace XUnitTest.MVCTest
{
    public class ProductControllerTest
    {
        private readonly Mock<IGenercRepository<Product>> _myMock;

        private readonly ProductsController _productsController;

        private List<Product> products;
        
        public ProductControllerTest()
        {
            _myMock = new Mock<IGenercRepository<Product>>();//burda constructorda MockBehaviour vere bilerik loose var strict var loose olanda reponun setupini vermesekde isdeyecek ama strictde mutleq setup olmalidi
            _productsController = new ProductsController(_myMock.Object);
            products = new List<Product>() { new Product() {ProductId = 1,CategoryId = 1, ProductName = "Samsung", UnitPrice = 100, UnitsInStock = 200 }, 
                new Product() { ProductId = 2, CategoryId = 2, ProductName = "Apple", UnitPrice = 102, UnitsInStock = 201 } };
        }

        [Fact]
        public void Index_ActionExecute_ReturnView()
        {
            var result = _productsController.Index();

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void Index_ActionExecute_ReturnProductList()
        {
            _myMock.Setup(repo => repo.GetAll()).Returns(products);

            var result = _productsController.Index();

            var viewResult = Assert.IsType<ViewResult>(result);

            var productList = Assert.IsAssignableFrom<IEnumerable<Product>>(viewResult.Model);

            Assert.Equal(2,productList.Count());
        }

        [Fact]
        public void Details_IdIsNull_ReturnRedirectIndex()
        {
            var result = _productsController.Details(null);

            var redirect = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Index", redirect.ActionName);
        }

        [Fact]
        public void Details_IdInvalid_ReturnNotFound()
        {
            Product product = null;

            _myMock.Setup(x => x.GetById(0)).Returns(product);

            var result = _productsController.Details(0);

            var viewResult = Assert.IsType<NotFoundResult>(result);

            Assert.Equal(404, viewResult.StatusCode);
        }

        [Theory]
        [InlineData(1)]
        public void Details_IdValid_ReturnProduct(int productId)
        {
            Product product = products.FirstOrDefault(x => x.ProductId == productId);
            _myMock.Setup(x => x.GetById(productId)).Returns(product);

            var result = _productsController.Details(productId);

            var viewResult = Assert.IsType<ViewResult>(result);

            var resultProduct = Assert.IsAssignableFrom<Product>(viewResult.Model);

            Assert.Equal(product.ProductId, resultProduct.ProductId);
            Assert.Equal(product.ProductName, resultProduct.ProductName);
        }

        [Fact]
        public void Create_ActionExecute_ReturnView()
        {
            var result = _productsController.Create();

            Assert.IsType<ViewResult>(result);
        }

        [Fact]
        public void CreatePost_InvalidModelState_ReturnView()
        {
            _productsController.ModelState.AddModelError("ProductName","Name Is required");

            var result = _productsController.Create(products.First());

            var viewResult = Assert.IsType<ViewResult>(result);

            Assert.IsType<Product>(viewResult.Model);
        }

        [Fact]
        public void CreatePost_ValidModelState_RedirectToIndex()
        {
            var result = _productsController.Create(products.First());

            var redirect = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Index", redirect.ActionName);
        }

        [Fact]
        public void CreatePost_ValidModelState_CreateMethodExecute()
        {
            Product newProduct = null;

            _myMock.Setup(repo => repo.Add(It.IsAny<Product>())).Callback<Product>(x=> newProduct = x);

            var result = _productsController.Create(products.First());

            _myMock.Verify(repo => repo.Add(It.IsAny<Product>()), Times.Once);

            Assert.Equal(products.First().ProductId,newProduct.ProductId);
        }

        [Fact]
        public void CreatePost_InvalidModelState_NeverCreateExecute()
        {
            _productsController.ModelState.AddModelError("ProductName","Name is required");

            var result = _productsController.Create(products.First());

            _myMock.Verify(repos => repos.Add(It.IsAny<Product>()), Times.Never);
        }

        [Fact]
        public void Edit_IdIsNull_RedirectToIndex()
        {
            var result = _productsController.Edit(null);

            var redirect = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Index",redirect.ActionName);
        }

        [Theory]
        [InlineData(0)]
        public void Edit_IdInvalid_RedirectNotFound(int productId)
        {
            Product product = null;
            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsController.Edit(productId);

            var redirect = Assert.IsType<NotFoundResult>(result);

            Assert.Equal(404,redirect.StatusCode);
        }

        [Theory]
        [InlineData(1)]
        public void Edit_ActionExecute_ReturnProduct(int productId)
        {
            var product = products.First(x =>x.ProductId == productId);

            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsController.Edit(productId);

            var viewResult = Assert.IsType<ViewResult>(result);

            var resultProduct = Assert.IsAssignableFrom<Product>(viewResult.Model);

            Assert.Equal(product.ProductId,resultProduct.ProductId);
        }

        [Theory]
        [InlineData(1)]
        public void EditPost_IdNotEqualProduct_ReturnNotFount(int productId)
        {
            var result = _productsController.Edit(2,products.Find(x=>x.ProductId == productId));

            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [InlineData(1)]
        public void EditPost_InvalidModelState_ReturnView(int productId)
        {
            _productsController.ModelState.AddModelError("ProductName","Name is required");

            var result = _productsController.Edit(productId, products.First(x=>x.ProductId == productId));

            var viewResult = Assert.IsType<ViewResult>(result);

            Assert.IsType<Product>(viewResult.Model);
        }

        [Theory]
        [InlineData(1)]
        public void EditPost_ValidModelState_RedirectToIndex(int productId)
        {
            var result = _productsController.Edit(productId,products.First(x=>x.ProductId == productId));

            var redirect = Assert.IsType<RedirectToActionResult>(result);

            Assert.Equal("Index",redirect.ActionName);
        }   

        [Theory]
        [InlineData(1)]
        public void EditPost_ValidModelState_EditExecute(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);

            _myMock.Setup(repo => repo.Update(product));

            _productsController.Edit(productId,product);

            _myMock.Verify(repo => repo.Update(It.IsAny<Product>()),Times.Once);
        }


        [Fact]
        public void Delete_IdIsNull_ReturnNotFound()
        {
            var result = _productsController.Delete(null);

            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [InlineData(0)]
        public void DeleteIdIsNotEqualProduct_ReturnNotFound(int productId)
        {
            Product product = null;

            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsController.Delete(productId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [InlineData(1)]
        public void Delete_MethodExecute_ReturnView(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);

            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsController.Delete(productId);

            var viewResult = Assert.IsType<ViewResult>(result);

            Assert.IsAssignableFrom<Product>(viewResult.Model);
        }

        [Theory]
        [InlineData(1)]
        public void DeleteConfirmed_Execute_ReturnRedirectToIndex(int productId)
        {
            var result = _productsController.DeleteConfirmed(productId);

            Assert.IsType<RedirectToActionResult>(result);
        }

        [Theory]
        [InlineData(1)]
        public void DeleteConfirmed_Execute_DeleteMethodExecute(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);

            _myMock.Setup(repo => repo.Delete(product));

            _productsController.DeleteConfirmed(productId);

            _myMock.Verify(x=>x.Delete(It.IsAny<Product>()),Times.Once);
        }

        [Theory]
        [InlineData(1)]
        public void ProductExist_Execute_ReturnTrue(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);
            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsController.ProductExists(productId);

            Assert.True(result);
        }

        [Theory]
        [InlineData(0)]
        public void ProductExist_Execute_ReturnFalse(int productId)
        {
            Product product = null;
            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsController.ProductExists(productId);

            Assert.False(result);
        }

    }
}
