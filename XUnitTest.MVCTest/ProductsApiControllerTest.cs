﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using XUnitTest.MVC.Controllers;
using XUnitTest.MVC.GR;
using XUnitTest.MVC.Models;

namespace XUnitTest.MVCTest
{
    public class ProductsApiControllerTest
    {
        private readonly Mock<IGenercRepository<Product>> _myMock;
        private readonly ProductsApiController _productsApiController;

        private List<Product> products;

        public ProductsApiControllerTest()
        {
            _myMock = new Mock<IGenercRepository<Product>>();
            _productsApiController = new ProductsApiController(_myMock.Object);
            products = new List<Product>() { new Product() {ProductId = 1,CategoryId = 1, ProductName = "Samsung", UnitPrice = 100, UnitsInStock = 200 },
                new Product() { ProductId = 2, CategoryId = 2, ProductName = "Apple", UnitPrice = 102, UnitsInStock = 201 } };
        }

        [Fact]
        public void GetProducts_ActionExecute_ReturnOkResultWithProducts()
        {
            _myMock.Setup(repo => repo.GetAll()).Returns(products);

            var result = _productsApiController.GetProducts();

            var okResult = Assert.IsType<OkObjectResult>(result);

            var returnProducts = Assert.IsAssignableFrom<IEnumerable<Product>>(okResult.Value);

            Assert.Equal(2,returnProducts.Count());
        }

        [Theory]
        [InlineData(0)]
        public void GetProduct_IdInavlid_ReturnNotFound(int productId)
        {
            Product product = null;
            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsApiController.GetProduct(productId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [InlineData(1)]
        public void GetProduct_IdValid_ReturnOkResultWithProduct(int productId)
        {
            var product = products.Where(x => x.ProductId == productId).FirstOrDefault();
            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsApiController.GetProduct(productId);

            var okResult = Assert.IsType<OkObjectResult>(result);

            var returnProduct = Assert.IsAssignableFrom<Product>(okResult.Value);

            Assert.Equal(product, returnProduct);
        }

        [Theory]
        [InlineData(1)]
        public void PutProduct_IdIsNotEqual_ReturnBadRequest(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);

            var result = _productsApiController.PutProduct(2,product);

            Assert.IsType<BadRequestResult>(result);
        }

        [Theory]
        [InlineData(1)]
        public void PutProduct_ActionExecute_ReturnNoContent(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);

            _myMock.Setup(repo => repo.Update(product));

            var result = _productsApiController.PutProduct(productId,product);

            _myMock.Verify(x=>x.Update(product),Times.Once);

            Assert.IsType<NoContentResult>(result);
        }

        [Fact]
        public void PostProduct_ActionExecute_ReturnCreatedAtAction()
        {
            var product = products.First();

            _myMock.Setup(repo => repo.Add(product));

            var result = _productsApiController.PostProduct(product);

            var createActionResult = Assert.IsType<CreatedAtActionResult>(result);

            _myMock.Verify(x=>x.Add(product),Times.Once);

            Assert.Equal("GetProduct",createActionResult.ActionName);
        }

        [Theory]
        [InlineData(0)]
        public void DeleteProduct_IdInvalid_ReturnNotFound(int productId)
        {
            Product product = null;
            _myMock.Setup(repo => repo.GetById(productId)).Returns(product);

            var result = _productsApiController.DeleteProduct(productId);

            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [InlineData(1)]
        public void DeleteProduct_ActionExecute_ReturnNoContent(int productId)
        {
            var product = products.First(x=>x.ProductId == productId);

            _myMock.Setup(x=>x.GetById(productId)).Returns(product);

            _myMock.Setup(repo => repo.Delete(product));

            var result = _productsApiController.DeleteProduct(productId);

            _myMock.Verify(x=>x.Delete(product),Times.Once);

            Assert.IsType<NoContentResult>(result);
        }


    }
}
