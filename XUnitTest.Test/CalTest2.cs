﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using XUnitTest.App;
using XUnitTest.App.services;

namespace XUnitTest.Test
{
    public class CalTest2
    {
        private Calculator calculator;

        private Mock<ICalculatorService> myMock;


        public CalTest2()
        {
            myMock = new Mock<ICalculatorService>();
            calculator = new Calculator(myMock.Object);
        }

        
        [Theory]
        [InlineData(2,3,5)]
        public void Sum_SimpleValues_ReturnTotalValue(int a,int b,int expectedTotal)
        {
            myMock.Setup(x => x.Add(a,b)).Returns(expectedTotal);

            var actualTotal = calculator.Add(a,b);

            Assert.Equal(expectedTotal,actualTotal);

        }

        [Theory]
        [InlineData(0,5)]
        public void Multip_ZeroValue_ThrowExceptiomn(int a,int b)
        {
            myMock.Setup(x => x.Multiple(a, b)).Throws(new Exception("a=0 ola bilmez"));

            Exception exception =  Assert.Throws<Exception>(() => calculator.Multiple(a, b));

            Assert.Equal("a=0 ola bilmez",exception.Message);
        }


        [Theory]
        [InlineData(2, 5,10)]
        public void Multip_SimpleValues(int a, int b,int expectedValue)
        {
            int actualMultip = 0;

            myMock.Setup(x => x.Multiple(It.IsAny<int>(), It.IsAny<int>())).Callback<int,int>((x,y) => actualMultip = x*y );//it.kisAny<type> verende ancaq o tip deyer gire biler
            //callback extra metod calistirmaq ucun isdifade olunur
            calculator.Multiple(a,b);

            Assert.Equal(expectedValue,actualMultip);

            calculator.Multiple(5,20);

            Assert.Equal(100,actualMultip);


        }

    }
}
