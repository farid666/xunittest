﻿using Moq;
using System.Collections.Generic;
using Xunit;
using XUnitTest.App;
using XUnitTest.App.services;

namespace XUnitTest.Test
{
    public class CalculatorTest
    {
        private readonly Calculator calculator1;
        private Mock<ICalculatorService> myMock;
       
        public CalculatorTest()
        {

          
            myMock = new Mock<ICalculatorService>();
            this.calculator1 = new Calculator(myMock.Object);
            //this.calculator1 = new Calculator(); //eger bir cclasi bir nece defe cagiririqsa onu 1 defe constructorda teyin etmek daha elverislidi her yerde isdifade ucun
        }
/*
        [Fact] //bu annotasiya bunun test metodu oldugunu VS ya basa salir testexplorerdde gosderir ve anvcaq parametr almiyan metodalara qeyd olunur
        public void AddTest()
        {
            //Arrange -- deyiskenleri initialize etdiyimiz ve obyekt yaratdgimiz yerdir

            int a = 5;
            int b = 20;

            //var calculator = new Calculator();


            //Act -- bizim yuxaridaki initialize etdiuyimiz deyerleri isletdiyimiz yerdi

            var total = calculator1.Add(a,b);

            //Assert -- neticeni yoxladigimiz yerdi dogrulama yeri

            Assert.Equal<int>(25,total);


            //metodlar

            //Assert.Contains("Ali","Ali");  //birinci gozlenilen 2 ci gelen neticedo yoxlayir gizlenilen gelen neticede varmi

            var names = new List<string>() { "Ali", "Omar", "Nahid" };

            //Assert.Contains(names, x=>x.Contains("Ali")); // bu yoxluyurki Ali adi names icinde var ya yox olsa kecir testdem
            //Assert.DoesNotContain(names, x => x.Contains("Ali"));

            //Assert.True(5 > 2); // butodlar werte gore yoxluyur sert truedusa kecir
            //Assert.False(5 > 2); // wert falsedusa kecir

            //Assert.True("".GetType()==typeof(string)); // burda tiplerine gire muqayise edir tekce int yox her weyi muqayise etmek olar


            //var regex = "^dog"; //bu regexde dog ile basdayir ya yox onu bildirir
            //Assert.Matches(regex,"dog"); //bu regex gozdediyimiz neticedi dog ise metoddan gelen olacaq
            //Assert.DoesNotMatch(regex,"dog");


            //Assert.StartsWith("bir","bir masal");//bu ise yoxluyurki biur sozunnen absdiyir ya yox true verir basdiyirsa kecir
            //Assert.EndsWith("masal","bur masal");//burda yoxluyurki masalnan bitib ya yoxy

            //Assert.Empty(new List<string>()); //bu metod Ienumerable in bos olub olmadigini yoxluyur bosdusa kecir testi
            //Assert.NotEmpty(new List<string>() { "Ali"}); //bu ise bos deyilse testi kecir

            //Assert.InRange<int>(10,2,20); // burda baxir 10 2 ve 20 arasinda yerlesir ya yox
            //Assert.NotInRange();// bu metod yuxaridakinin eksini

            //Assert.Single(new List<string>() {"Ali" }); // bu yoxluyur listimizde  1dene deyer var ya yox


            //Assert.IsType<string>("Ali");//bu tpini yoxluyur eynidise kecir testden
            //Assert.IsNotType(); // uxaridakinin eksi

            //Assert.IsAssignableFrom<IEnumerable<string>>(new List<string>);//bnu tmetod 1 tipin 1 tipe referans verib vermiyeceyini yoxluyur burda List Ienumerabledan implement edib deye kecir tesden

            //string a = null;
            //Assert.Null(a); // bu metod icine verilen deyerin nulll olub olmadigini yoxluyur
            //Assert.NotNull(a); bu ise yuxardakinin eksine

            


        }*/

        [Theory]// bu annotasiya inlinedataynan bir yazilir ve metoda parametr vermekde isdifade eolunur
        [InlineData(1,2,3)] //isdenilen qeder inlinedata vere bilerik alt alta hamsini test eder
        [InlineData(12,2,14)]
        public void Add_SimpleValues_ReturnTotal(int a,int b,int expectedTotal)//adlardirmalari bele vermek duzdu 1ci mhansi metoddu 2ci ne deyer girilir 3 cu geri ne donur
        {
            //var calculator = new Calculator();

           
            myMock.Setup(x => x.Add(a,b)).Returns(expectedTotal);

            var actualTotal = calculator1.Add(a, b);

            Assert.Equal(expectedTotal,actualTotal);

            myMock.Verify(x => x.Add(a,b),Times.Once);//bu metodun 1 defe cagirilmasini yoxluyur cagirilirsa testden kecir verify metodun isdeyib isdemdoiyini yoxluyur
        }



    }
}
